import sys
from explorer.assets.player import Player
from explorer.assets.bag import Bag, BagVolumeError


def find(object_name):
    print("Finding the object", object_name)
    if object_name == "ball":
       return object_name
    return None

def add(first, second):
    return first + second


def main():
    print("Let's start the explorer game")
    try:
        player_1 = Player("Bob")
        player_1.introduce_yourself()

        player_2 = Player("Eve")
        player_2.introduce_yourself()

        print("Max volume of bags is", Bag.MAX_VOLUME, "liter.")
        print("Max volume of bags is", Bag.max_volume_in_lbs(), "lbs.")

        bag = Bag(volume=11.9)
        print("The max volume is still:", bag.MAX_VOLUME)
        bag.MAX_VOLUME = 42.0
        print("The max volume is now:", bag.MAX_VOLUME)

        print("Max volume of bags is finally", Bag.MAX_VOLUME)

        find("balletje")

    except BagVolumeError as e:
        print("Couldn't create a bag")
        print(e)
    except Exception as e:
        print("An exceptional case")
        print(e)
        sys.exit(1)



