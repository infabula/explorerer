# API
class Player:
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    # no setter for name: name is now immutable

    def introduce_yourself(self):
        '''Print a message to the stdout'''
        message = "Hi, I am {name}".format(name=self.name)
        print(message)


if __name__ == "__main__":
    player1 = Player("Bob")