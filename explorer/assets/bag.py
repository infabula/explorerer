class BagVolumeError(Exception):
    pass


class Bag:
    '''Contains game items.'''

    MAX_VOLUME =      30.0  # liters

    def __init__(self, volume):
        self.volume = volume

    @staticmethod
    def max_volume_in_lbs():
        '''Returns the maximum volume in lbs.'''
        return Bag.MAX_VOLUME * 2.204622622

    @property
    def volume(self):
        return self._volume

    @volume.setter
    def volume(self, volume):
        '''sets the volume. Raises a BagVolumeError.'''
        print("Volume.setter")
        if volume >= 0.0:
            self._volume = volume
        else:
            raise BagVolumeError("Volume can not be negative")
