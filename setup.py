from setuptools import setup, find_packages


setup(
    name='explorer',
    version='0.0.2',
    url='https://explorer.notexixting',
    author='Anonymous',
    author_email='anonymous@example.org',
    packages=['explorer']
)
